<?php
session_start();

if (!isset($_SESSION['usuario'])) header('Location: login.php');

include("BD.php");

if($_SERVER['REQUEST_METHOD'] == 'POST')
{

    $cuenta = $_REQUEST['numCuenta'];
    $nombre = $_REQUEST['nombre'];
    $primer_apellido = isset($_REQUEST['primerAp']) ? $_REQUEST['primerAp'] : null;
    $segundo_apellido = isset($_REQUEST['segundoAp']) ? $_REQUEST['segundoAp'] : null;
    $genero = isset($_REQUEST['genero']) ? $_REQUEST['genero'] : null;
    $fecha_nacimiento = isset($_REQUEST['fechaNacimiento']) ? $_REQUEST['fechaNacimiento'] : null;
    $password = $_REQUEST['password'];

        if( array_key_exists($cuenta, $usuarios) || array_key_exists($password, $usuarios))
        {
            ?>
                <script>alert("¡El alumno ya existe!");</script>
            <?php

            
        }
        else
        {
            $usuarios[$cuenta]= ['numero_cuenta' => $cuenta, 'nombre' => $nombre,
            'primer_apellido' => $primer_apellido, 'segundo_apellido' => $segundo_apellido, 
            'password' => $password, 'genero' => $genero, 'fecha_nacimiento' => $fecha_nacimiento];

            ?>
                <script>alert("¡Los datos se registraron CORRECTAMENTE!");</script>
            <?php

            

        }        
    
}