<?php
session_start();

if (!isset($_SESSION['usuario'])) header('Location: login.php');

include("BD.php");

$nombre = $_SESSION['usuario']['nombre'];
$apellido = $_SESSION['usuario']['primer_apellido'];
$cuenta = $_SESSION['usuario']['numero_cuenta'];
$fecha_n = $_SESSION['usuario']['fecha_nacimiento'];