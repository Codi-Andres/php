<?php

include("BD.php");
    
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        if (isset($_POST['usuario']) && isset($_POST['password'])){
            $usuario = trim($_REQUEST['usuario']);
            $contraseña = trim($_REQUEST['password']);
            for ($i = 0; $i < count ($usuarios); $i++){
                if (($usuarios [$i]['numero_cuenta'] == $usuario) && ($usuarios [$i]['password'] == $contraseña)){
                    // ¡encontramos al usuario!
                    session_start();
                    $_SESSION['usuario']= $usuarios[$i];
                    header ("Location: ../view/info.php");
                    die();
                }
                else 
                {
                    ?>
                      <script>alert("¡Introduce cuenta y contraseña correctos!");</script>
                    <?php
                }
            }
        }   
        else
        {
            ?>
                <script>alert("¡Rellene los campos!");</script>
            <?php
        }
    }

?>