<?php include("../modal/info.php"); ?>


<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sitio prueba</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="http://localhost/DGTIC/Cursos/PHP/ejercicio_formulario/public/css/estilos.css" />
</head>

<body>

<div id="menu">
        <nav class="navbar navbar-expand-lg navbar-light bg-primary">
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="../view/info.php" style="color: white">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../view/formulario.php" style="color: white">Registrar Alumnos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../modal/cerrar_sesion.php" style="color: white">Cerrar Sesión</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <br />
    <br />

    <div id="container">

      <div id="container-info-usuario">
        <h1>Usuario Autenticado</h1>
        <div id="cuadro">
          <div id="cuadro-header">
            <p><?= $nombre," ",$apellido; ?> </p>
          </div>
          <div id="cuadro-container">
            <h3>Informacion</h3>
            <p>Numero de cuenta: <?= $cuenta ?></p>
            <p>Fecha de Nacimiento: <?= $fecha_n ?></p>
          </div>
        </div>
      </div>

      <div id="container-info-datos">
        <h1>Datos Guardados:</h1>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nombre</th>
              <th scope="col">Fecha de Nacimiento</th>
            </tr>
          </thead>
          <tbody>
            <?php
            for ($i = 0; $i < count($usuarios); $i++)
            {
              ?>
                <tr>
                  <td><?php echo $i ?></td>
                  <td><?php echo $usuarios[$i]['nombre'] ?></td>
                  <td><?php echo $usuarios[$i]['fecha_nacimiento'] ?></td>
                </tr>
            <?php       
            }
            ?>
          </tbody>
        </table>
      </div>
</div>

<br />
</body>
</html>