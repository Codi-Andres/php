<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>Sitio prueba</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="http://localhost/DGTIC/Cursos/PHP/ejercicio_formulario/public/css/estilos.css" />
</head>
<body>
<section class="vh-100" style="background-color: #508bfc;">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-12 col-md-8 col-lg-6 col-xl-5">
        <div class="card shadow-2-strong" style="border-radius: 1rem;">
          <form method="post" class="card-body p-5 text-center">

            <h3 class="mb-5">Sign in</h3>

            <div class="form-outline mb-4">
              <input type="text" name="usuario" class="form-control form-control-lg" />
              <label class="form-label" for="typeEmailX-2">Numero de Cuenta</label>
            </div>

            <div class="form-outline mb-4">
              <input type="password" name="password" class="form-control form-control-lg" />
              <label class="form-label" for="typePasswordX-2">Contraseña</label>
            </div>

            <button class="btn btn-primary btn-lg btn-block" type="submit" name="ingresar" id=ingresar">Login</button>

            <hr class="my-4">

          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- incluir archivo de modal de login  -->
<?php include("../modal/login.php"); ?>
</body>
</html>