<?php include("../modal/formulario.php"); ?>


<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sitio prueba</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="http://localhost/DGTIC/Cursos/PHP/ejercicio_formulario/public/css/estilos.css" />
</head>

<body>

    <div id="menu">
        <nav class="navbar navbar-expand-lg navbar-light bg-primary">
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="../view/info.php" style="color: white">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../view/formulario.php" style="color: white">Registrar Alumnos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../modal/cerrar_sesion.php" style="color: white">Cerrar Sesión</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <br />
    <br />

    <div id="container">
        <form method="post">
            <table>
                <tr>
                    <td>Numero de cuenta</td>
                    <td><input type="number" name="numCuenta"  placeholder="Número de cuenta"></td>
                </tr>
                <tr>
                    <td>Nombre</td>
                    <td><input type="text" name="nombre"  placeholder="Nombre"></td>
                </tr>
                <tr>
                    <td>Primer Apellido</td>
                    <td><input type="text" name="primerAp"  placeholder="Primer Apellido"></td>
                </tr>
                <tr>
                    <td>Segundo Apellido</td>
                    <td><input id="curp" type="text" name="segundoAp"  placeholder="Segundo Apellido"></td>
                </tr>
                <tr>
                    <td>Género</td>
                    <td>
                       <input type="radio" name="genero" value="M" > Hombre <br/>
                       <input type="radio" name="genero" value="F" > Mujer <br/>
                       <input type="radio" name="genero" value="O" > Otro <br/>
                    </td>
                </tr>
                <tr>
                    <td>Fecha de Nacimiento</td>
                    <td><input type="date" name="fechaNacimiento"></td>
                </tr>
                <tr>
                    <td>Contraseña</td>
                    <td><input type="text" name="password" placeholder="Contraseña" ></td>
                </tr>
            </table>
            <br/>        
            <p>
               <button type="submit" name="registrar">Registrar</button>
            </p>       
        </form>
    </div>
    <br />
</body>
</html>
