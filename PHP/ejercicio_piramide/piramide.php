<?php

// al presionar boton aceptar
if(isset($_POST['aceptar'])){
    
	// guardando valor del input tamaño_dibujo y del radio tipo_dibujo en 		variables
	$numero=$_POST['tamanio_dibujo'];
	$dibujo=$_POST['tipo_dibujo'];

    // restringiendo la variable $numero 
    if($numero < 2 || $numero > 30){
        echo "<h2 style='background-color:red; text-align:center'> Tamaño del dibujo incorrecto. Favor de intentar nuevamente!</h2>";
    } else {

        // colocando el dibujo en un div
        ?> <div style="display:grid; justify-content:space-evenly;"> <?php

        // imprimiendo piramide
        if ($dibujo == "Piramide") {
            piramide($numero);
        }

        // imprimiendo rombo
        if ($dibujo == "Rombo") {
            piramide($numero);
            reflejo_piramide($numero);
        }

        ?> </div> <?php
    }
}

// funcion que muestra el triangulo
function piramide($n) {
    return array_map(
        function ($i) use ($n) {
            echo "<pre style='margin:0px'>".str_repeat(" ", $n-$i-1).str_repeat("*", $i+$i+1)."</pre>";
        }, range(0, $n-1)
    );
}
// funcion que muestra un triangulo invertido 
function reflejo_piramide($n) {
    return array_map(
        function ($i) use ($n) {
            echo "<pre style='margin:0px'>".str_repeat(" ", $n-$i-1).str_repeat("*", $i+$i+1)."</pre>";
        }, range($n-2, 0)
    );
}

/*-
// imprime rombo usando solo ciclo for
for($t=0; $t < 10; $t++){// parte superior
	echo "<pre>".str_repeat(" ", 10-$t-1).str_repeat("*", $t+$t+1)."</pre>";
}
for($t=8; $t >= 0; $t--){// parte inferior
	echo "<pre>".str_repeat(" ", 10-$t-1).str_repeat("*", $t+$t+1)."</pre>";
}
*/
?>

</body>
</html>