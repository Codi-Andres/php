<?php

$str = $_REQUEST["q"];
// ^ inicio del string
// [],() encerrar contenido string
// A-z0-9\s de la A=A-Z y z=a-z y 0-9 y \s=espacios en blanco
// el resto de simbolos indican que esos mismos son permitidos
// {50,} el rango de caracteres permitidos {valor_inicial, valor_final}
// $ fin del string
if(preg_match('/^[(A-z\s)*]{50,}$/', $str))
{
    echo "<span style='color: green; opacity: .4;'>Longitud Correcta</span>";
}
else
{
    echo "<span style='color: red; opacity: .4;'>Longitud Incorrecta</span>";
}