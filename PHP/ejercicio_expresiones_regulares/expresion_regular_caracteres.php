<?php

$str = $_REQUEST["q"];

if(preg_match('/^(?:.*[@$%?ñ"\=¡¿\|\!\'¡\,\;\/\:\´\-_.\{\}]){1,}$/', $str))
{
    echo "<span style='color: green; opacity: .4;'>Correcta</span>";
}
else
{
    echo "<span style='color: red; opacity: .4;'>Incorrecta</span>";
}