

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio Expresiones regulares</title>
</head>

<body>
    <div id="container">
        <form method="post">
            <table>
                <tr>
                    <td>Email</td>
                    <td><input type="text" name="email" size="50" onkeyup="comprobarEmail(this.value)" placeholder="example@example.com"></td>
                    <td><span id="txtComprobarEmail"></span></td>
                </tr>
                <tr>
                    <td>CURP</td>
                    <td><input type="text" name="curp" size="50" onkeyup="comprobarCURP(this.value)" placeholder="Longitud 18 caracteres"></td>
                    <td id="txtComprobarCURP"></td>
                </tr>
                <tr>
                    <td>Descripción</td>
                    <td><input type="text" name="descripcion" size="50"  onkeyup="comprobarDescripcion(this.value)" placeholder="Solo palabras con longitud mayor de 50 caracteres "></td>
                    <td id="txtComprobarDescripcion"></td>
                </tr>
                <tr>
                    <td>Numero</td>
                    <td><input type="text" name="numero" size="50" onkeyup="comprobarNumero(this.value)" placeholder="Numeros decimales"></td>
                    <td id="txtComprobarNumero"></td>
                </tr>
                <tr>
                    <td>Caracteres especiales</td>
                    <td><input type="text" name="caracteres" size="50" onkeyup="comprobarCaracteres(this.value)" placeholder="Agrege texto con caracteres especiales [*\^.]"></td>
                    <td id="txtComprobarCaracteres"></td>
                </tr>
            </table>      
        </form>
    </div>
    <br />
</body>
<script>
function comprobarEmail(str) {
  if (str.length == 0) {
    document.getElementById("txtComprobarEmail").innerHTML = "";
    return;
  } else {
    // Crear un objeto XMLHttpRequest
    var xmlhttp = new XMLHttpRequest();
    // Crea la función que se ejecutará cuando la respuesta del servidor esté lista
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("txtComprobarEmail").innerHTML = this.responseText;
      }
    };
    // Envía la solicitud al archivo PHP 
    xmlhttp.open("GET", "expresion_regular_email.php?q=" + str, true);
    xmlhttp.send();
  }
}
function comprobarCURP(str) {
  if (str.length == 0) {
    document.getElementById("txtComprobarCURP").innerHTML = "";
    return;
  } else {
    // Crear un objeto XMLHttpRequest
    var xmlhttp = new XMLHttpRequest();
    // Crea la función que se ejecutará cuando la respuesta del servidor esté lista
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("txtComprobarCURP").innerHTML = this.responseText;
      }
    };
    // Envía la solicitud al archivo PHP 
    xmlhttp.open("GET", "expresion_regular_curp.php?q=" + str, true);
    xmlhttp.send();
  }
}
function comprobarDescripcion(str) {
  if (str.length == 0) {
    document.getElementById("txtComprobarDescripcion").innerHTML = "";
    return;
  } else {
    // Crear un objeto XMLHttpRequest
    var xmlhttp = new XMLHttpRequest();
    // Crea la función que se ejecutará cuando la respuesta del servidor esté lista
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("txtComprobarDescripcion").innerHTML = this.responseText;
      }
    };
    // Envía la solicitud al archivo PHP 
    xmlhttp.open("GET", "expresion_regular_descripcion.php?q=" + str, true);
    xmlhttp.send();
  }
}
function comprobarNumero(str) {
  if (str.length == 0) {
    document.getElementById("txtComprobarNumero").innerHTML = "";
    return;
  } else {
    // Crear un objeto XMLHttpRequest
    var xmlhttp = new XMLHttpRequest();
    // Crea la función que se ejecutará cuando la respuesta del servidor esté lista
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("txtComprobarNumero").innerHTML = this.responseText;
      }
    };
    // Envía la solicitud al archivo PHP 
    xmlhttp.open("GET", "expresion_regular_numero.php?q=" + str, true);
    xmlhttp.send();
  }
}
function comprobarCaracteres(str) {
  if (str.length == 0) {
    document.getElementById("txtComprobarCaracteres").innerHTML = "";
    return;
  } else {
    // Crear un objeto XMLHttpRequest
    var xmlhttp = new XMLHttpRequest();
    // Crea la función que se ejecutará cuando la respuesta del servidor esté lista
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("txtComprobarCaracteres").innerHTML = this.responseText;
      }
    };
    // Envía la solicitud al archivo PHP 
    xmlhttp.open("GET", "expresion_regular_caracteres.php?q=" + str, true);
    xmlhttp.send();
  }
}

</script>
</html>